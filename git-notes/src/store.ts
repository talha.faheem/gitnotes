import { createStore,compose,applyMiddleware } from 'redux';
import thunk from 'redux-thunk'; 
import { createLogger } from 'redux-logger'
import allReducers from './reducers';


const composeEnhancers = (<any>window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const logger = createLogger({
  collapsed: true,
});
const store = createStore(
  allReducers,
  composeEnhancers(applyMiddleware(thunk, logger))
);
 // store.subscribe(() => store.getState())
export default store;