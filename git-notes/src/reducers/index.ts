import { combineReducers } from "redux";
import gistsData from "./gistsData";
import authToken from "./authToken";
const allReducers = combineReducers({
  gistList: gistsData,
  authToken
});
export default allReducers;
