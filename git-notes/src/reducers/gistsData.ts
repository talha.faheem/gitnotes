/* import data from "../data.json"; */
import * as R from "ramda";
import {
  API_SUCCESS,
  API_FAILURE,
  INITIAL_REQUEST,
  DELETE_GIST,
  ADD_GISTS
} from "../constants";
const INITIAL_STATE = {
  isLoading: false,
  gistData: []
};
interface IGistList {
  id: string;
  url: string;
  files: object;
  public: boolean;
  created_at: string;
}
const gistsData = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case INITIAL_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case API_FAILURE:
      return {
        ...state,
        isLoading: false
      };
    case API_SUCCESS:
      return {
        ...state,
        gistData: action.payload,
        isLoading: false
      };
    case ADD_GISTS:
    console.log("HELLO ADD ME",action.payload)
      return {
        ...state,
        isLoading: false
        
      };
    case DELETE_GIST:
      console.log("DELETE ===>>>", state, action);
      return {
        ...state,
        //@ts-ignore
        gistData: R.reject(R.propEq("id", action.payload), state.gistData)
      };
    default:
      return state;
  }
};
export default gistsData;
