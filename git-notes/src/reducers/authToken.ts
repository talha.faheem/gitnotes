import { AUTH_TOKEN } from "../constants";
const INITIAL_STATE = {
  authToken : ""
};
const authToken = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case AUTH_TOKEN:
      return {
        ...state,
        authToken: action.payload
      };
    default:
      return state
      
  }
};
export default authToken;