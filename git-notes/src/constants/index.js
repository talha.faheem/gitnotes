export const API_SUCCESS = "API_SUCCESS";
export const API_FAILURE = "API_FAILURE"; 
export const INITIAL_REQUEST = "INITIAL_REQUEST";
export const AUTH_TOKEN = "AUTH_TOKEN";
export const DELETE_GIST = "DELETE_GIST";
export const ADD_GISTS = "ADD_GISTS"
export const LOGIN_TOKEN = window.localStorage.getItem("gist-token")