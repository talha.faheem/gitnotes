import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import './App.css';
import LoginModal from './components/LoginModal/LoginModal';
import store from './store';
import NotesList from './components/NotesDetail/NotesList';
import NotesPage from './components/NotesPage/NotesPage';
import MainHeader from './components/Header/MainHeader';
class App extends Component {
  render() {
    return (
      <Router>
        <Provider store={store}>
            <MainHeader />
            <Route exact={true} path="/" component={NotesList} />
            <Route exact={true} path="/Login" component={LoginModal} />
            <Route exact={true} path="/notebooks" component={NotesList} />
            <Route exact={true} path="/notebooks/:gistId" component={NotesPage} />
        </Provider>
      </Router>

    );
  }

}

export default App;
