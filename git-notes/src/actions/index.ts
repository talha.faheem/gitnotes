import * as R from "ramda";
import {
  API_FAILURE,
  INITIAL_REQUEST,
  API_SUCCESS,
  AUTH_TOKEN,
  DELETE_GIST,
  ADD_GISTS
} from "../constants";

const getFailure = (error: any) => {
  return {
    type: API_FAILURE,
    payload: error
  };
};
const getSuccess = (data: any) => {
  return {
    type: API_SUCCESS,
    payload: data
  };
};
const initialRequest = () => {
  return {
    type: INITIAL_REQUEST
  };
};
const addGistSuccess = (data: any) => {
  console.log("ADD GIST SUCESS", data);
  return {
    type: ADD_GISTS,
    payload: data
  };
};
const deleteData = (gistId: string) => {
  return {
    type: DELETE_GIST,
    payload: gistId
  };
};
const authToken = (data: any) => {
  console.log("DATA", data);
  return {
    type: AUTH_TOKEN,
    payload: data
  };
};
export const getAuthToken = (code: string) => {
  return (dispatch: any) => {
    fetch(`http://localhost:3001/api/oauth/access-token`, {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        code: code
      })
    })
      .then(response => response.json())
      .then(data => {
        let firstSplit = R.split("&", data);
        let tokenSplit = R.split("=", firstSplit[0]);
        // @ts-ignore
        /*  console.log("MY DATA ACTION", tokenSplit[1]); */
        window.localStorage.setItem("gist-token", tokenSplit[1]);
        dispatch(authToken(tokenSplit[1]));
        /* dispatch(getSuccess(data)); */
      })
      .catch(err => {
        console.log("MY ERROR", err);
        /*  dispatch(getFailure(err)); */
      });
  };
};

export const getGistUser = () => {
  return (dispatch: any) => {
    fetch(`http://localhost:3001/api/gist/user`, {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        authToken: window.localStorage.getItem("gist-token")
      })
    })
      .then(response => response.json())
      .then(data => {
        let userData = R.split(",", data);
        let userName = R.split(":",userData[0])
        console.log("USER DATA :- ", userName[1]);
        // @ts-ignore
        window.localStorage.setItem("gistUser",userName[1])
        console.log(JSON.parse(data));

        //dispatch(getSuccess(data));
      })
      .catch(err => {
        console.log("MY ERROR", err);
        dispatch(getFailure(err));
      });
  };
};

export const getAllGist = (userName: string) => {
  return (dispatch: any) => {
    dispatch(initialRequest());
    console.log("HELLO DATA");
    fetch(`http://localhost:3001/api/gists/user/${userName}`, {
      method: "GET",
      headers: {
        "Content-type": "application/json"
      }
    })
      .then(response => response.json())
      .then(data => {
        // @ts-ignore
        console.log("MY DATA GIST LIST", data);
        dispatch(getSuccess(data));
      })
      .catch(err => {
        console.log("MY ERROR", err);
        dispatch(getFailure(err));
      });
  };
};
export const getGistById = (gistId: string) => {
  return (dispatch: any) => {
    dispatch(initialRequest());
    console.log("HELLO BY ID", gistId);
    fetch(`http://localhost:3001/api/gists/${gistId}`, {
      method: "GET",
      headers: {
        "Content-type": "application/json"
      }
    })
      .then(response => response.json())
      .then(data => {
        // @ts-ignore
        console.log("MY DATA BY ID", data);
        dispatch(getSuccess(data));
      })
      .catch(err => {
        console.log("MY ERROR", err);
        dispatch(getFailure(err));
      });
  };
};

export const addGist = (gistsObject: any) => {
  console.log("CREATE GIST 1", gistsObject);
  return (dispatch: any) => {
    console.log("CREATE GIST 2", gistsObject);
    dispatch(initialRequest());
    fetch(`http://localhost:3001/api/gists`, {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        description: gistsObject.title,
        files: {
          "demofile.txt": {
            content: `Please add you content Below.`
          }
        },
        authToken: window.localStorage.getItem("gist-token")
      })
    })
      .then(response => response.json())
      .then(data => {
        // @ts-ignore
        console.log("MY DATA BY ID", data);
        dispatch(addGistSuccess(data));
      })
      .catch(err => {
        console.log("MY ERROR", err);
        dispatch(getFailure(err));
      });
  };
};

export const editGistFile=(gistId:string, editObject: any)=>{
  return (dispatch: any) => {
    fetch(`http://localhost:3001/api/gist/file/${gistId}`, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        files: editObject,
        authToken: window.localStorage.getItem("gist-token")
      })
    })
      .then(response => response.json())
      .then(data => {
        // @ts-ignore
        console.log("MY DATA BY ID", data);
        /* dispatch(addGistSuccess(data)); */
      })
      .catch(err => {
        console.log("MY ERROR", err);
        dispatch(getFailure(err));
      });
  }
}

export const deleteGist = (gistId: string) => {
  console.log("GIST ID", gistId);
  return (dispatch: any) => {
    fetch(`http://localhost:3001/api/gists/${gistId}`, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        authToken: window.localStorage.getItem("gist-token")
      })
    })
      .then(response => response.json())
      .then(data => {
        // @ts-ignore
        console.log("MY DATA BY ID", data);
        dispatch(deleteData(gistId));
      })
      .catch(err => {
        console.log("MY ERROR", err);
        dispatch(getFailure(err));
      });
  };
};
