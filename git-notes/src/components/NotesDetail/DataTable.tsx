import React from 'react';
import { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import Share from '@material-ui/icons/Share';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import moment from 'moment';

interface IProps {
  gistList: any;
  deleteUserGist: (gistId: string) => void
}
const styles = (theme: any) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

function DataTable(props: IProps) {
  const { gistList,deleteUserGist } = props
  console.log("DATA TABLES ", gistList);
  //Object.keys(item.files).length
  return (
    <Fragment>
      <Paper >
        <Table>
          <TableHead>
            <TableRow>
              <TableCell >#</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Notes</TableCell>
              <TableCell>Created At</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              gistList.map((item: any, key: any) => (
                <TableRow key={key}>
                  <TableCell>{key + 1}</TableCell>
                  <TableCell > <Link style={{ color: "rgb(197, 17, 98)", textDecoration: "none" }} to={`/Notebooks/${item.id}`} > {item.description}</Link></TableCell>
                  <TableCell>Public</TableCell>
                  <TableCell>{Object.keys(item.files).length}</TableCell>
                  <TableCell>{moment(item.created_at).format("MM/DD/YYYY")}</TableCell>
                  <TableCell> <Edit /> <Share /> <Delete onClick={() => deleteUserGist(item.id)} style={{cursor:"pointer"}}/> </TableCell>
                </TableRow>
              ))
            }

          </TableBody>
        </Table>
      </Paper>
    </Fragment>
  )
}
export default DataTable;