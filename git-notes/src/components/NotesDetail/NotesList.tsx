import React, { Component } from "react";
import { connect } from 'react-redux';
import { getAllGist, deleteGist, getGistUser } from '../../actions';
import './notesList.css';
import AddNotesModal from "../AddNotesModal/AddNotesModal";
import NotesListInner from "./NoteListInner/NotesListInner";
import { bindActionCreators } from "redux";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Redirect } from "react-router";
import { LOGIN_TOKEN } from "../../constants";

type IProps = {
  gistList: any;
  getAllGist: (userName: string) => void;
  deleteGist: (gistId: string) => void;
  authToken: string;
  isLoading: boolean;
  getGistUser: () => void;
}
interface IState {
  addModal: boolean;
  gistsList: any;
  userName: string
}

class NotesList extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    {
      this.state = {
        addModal: false,
        gistsList: [],
        userName: ""
      }
      /* window.localStorage.getItem("gistUser") */
    }
    this.deleteUserGist = this.deleteUserGist.bind(this)
  }
  componentDidMount() {
    this.fetchGistList();
  }
  componentDidUpdate() {

  }
  addModalOpen = () => {
    this.setState({
      addModal: true
    })
  }
  addModalClose = () => {
    this.setState({
      addModal: false
    })
  }
  deleteUserGist(gistId: string) {
    console.log(this.props, "MY Talha")
    this.props.deleteGist(gistId);
    /* this.fetchGistList(); */
  }
  fetchGistList = () => {
    const { getAllGist } = this.props;

    let userName = window.localStorage.getItem("gistUser");
    console.log("USER NAME ", userName)
    // @ts-ignore
    getAllGist("MuhammadTalhaFaheem");
    /*  */
  }
  render() {
    console.log("LOGIN_TOKEN",LOGIN_TOKEN);
    const { gistList, isLoading } = this.props;
    const { addModal } = this.state;
    return (
      <div className="notesMain">
        {isLoading &&
          <CircularProgress className="progress" color="secondary" />
        }
        {((gistList && LOGIN_TOKEN) && !isLoading) && <NotesListInner
          gistList={gistList}
          deleteUserGist={this.deleteUserGist}
          addModalOpen={() => this.addModalOpen()}
        /> /* : <Redirect to="/Login" /> */}
        <AddNotesModal
          addModal={addModal}
          addModalClose={() => this.addModalClose()}
        />
      </div>
    );
  }
}
function mapStateToProps(state: any) {
  console.log("MAP", state)
  return {
    isLoading: state.gistList.isLoading,
    gistList: state.gistList.gistData,
    authToken: state.authToken.authToken
  }
}
function mapDispatchToProps(dispatch: any) {
  return bindActionCreators({
    getAllGist: getAllGist,
    deleteGist: deleteGist
  }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(NotesList);