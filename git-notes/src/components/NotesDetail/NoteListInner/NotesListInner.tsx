import React from 'react';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import DataTable from "../DataTable";
import BookImage from '@material-ui/icons/Book';
import Paper from '@material-ui/core/Paper';
interface IProps {
  addModalOpen: () => void;
  deleteUserGist: (gistId: string) => void;
  gistList: any;

}
function NotesListInner(props: IProps) {

  const {
    addModalOpen,
    gistList,
    deleteUserGist
  } = props
  return (
    <Grid container lg={10} className="notesGrid">
      <Grid container alignItems="center" lg={12} direction="row" className="listGrid">
        <Grid item lg={6} >
          <h4 className="headerHeading">My Notebook</h4>
        </Grid>
        <Grid item lg={6} style={{ display: "flex" }} direction="row" justify="flex-end">
          <Button
            className="newFileButton"
            variant="contained"
            color="secondary"
            onClick={() => addModalOpen()}
            style={{ marginRight: "10px", backgroundColor: "#c51162" }}>
            ADD NEW <BookImage />
          </Button>
        </Grid>
      </Grid>
      <Grid item lg={12} >
        {(gistList.length !== 0)&& <DataTable
          gistList={gistList}
          deleteUserGist={deleteUserGist}
        />
        
        }

      </Grid>
    </Grid>
  );
}
export default NotesListInner