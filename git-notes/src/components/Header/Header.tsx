import * as React from "react";
import Grid from '@material-ui/core/Grid';
import "./header.css"
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import PersonPin from "@material-ui/icons/PersonPin";

import { LOGIN_TOKEN } from '../../constants'
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Popper from "@material-ui/core/Popper";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
interface IProps {
  anchorEl: "" | null;
  anchorClick: (e: any) => void;
  anchorClose: () => void;
}
function Header(props: IProps) {
  const {
    anchorEl,
    anchorClick,
    anchorClose } = props
  return (
    <div className="header">
      <Grid
        container
        className="gridContainer"
        direction="row"
        alignItems="center"
      >
        <Grid item lg={6}>
          <h4 className="headerHeading">Github Notebook</h4>
        </Grid>
        <Grid className="headerButtonFlex" item lg={6} justify="flex-end">
          <Button
            variant="contained"
            className="headerButton"
            color="secondary"
            style={{ marginRight: "10px", backgroundColor: "#c51162" }}>
            HOME
          </Button>
          {!LOGIN_TOKEN && <Button
            variant="contained"
            className="headerButton"
            color="secondary"
            style={{ marginRight: "10px", backgroundColor: "#c51162" }}>
            LOGIN
          </Button>}
          {LOGIN_TOKEN && <React.Fragment>
            <Button
              variant="contained"
              className="headerButton"
              color="secondary"
              style={{ marginRight: "10px", backgroundColor: "#c51162" }}>
              NOTEBOOKS
            </Button>
            <div>
              <Fab size="small"
                aria-owns={anchorEl ? 'logout-menu' : undefined}
                aria-haspopup="true"
                onClick={anchorClick}
              >
                <PersonPin />
              </Fab>
              <Menu
                id="logout-menu"
                open={Boolean(anchorEl)}
                onClose={anchorClose}
              >
                <MenuItem>Profile</MenuItem>
                <MenuItem>LOGOUT</MenuItem>
              </Menu>
            </div>

          </React.Fragment>}

        </Grid>
      </Grid>
    </div>

  );
}

export default Header;