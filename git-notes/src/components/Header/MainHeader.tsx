import React from 'react';
import Header from './Header';

interface IProps {

}
interface IState {
  anchorEl: "" | null
}
class MainHeader extends React.Component<IProps,IState>{
  constructor(props: IProps) {
    super(props);
    this.state = {
      anchorEl: ""
    }
  }
  anchorClick = (event: any) => {
    this.setState({ anchorEl: event.currentTarget });
  }
  anchorClose = () => {
    this.setState({ anchorEl: null });
  };
  render() {
    return (
      <React.Fragment>
        <Header 
          anchorEl ={this.state.anchorEl}
          anchorClick = {this.anchorClick}
          anchorClose = {this.anchorClose}
        />
      </React.Fragment>
    );
  }
}
export default MainHeader;