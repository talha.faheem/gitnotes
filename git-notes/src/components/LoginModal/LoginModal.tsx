import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import GitHubLogin from 'react-github-login';
import { getAuthToken, getGistUser } from "../../actions";
import serverUrl from '../../constants/serverUrl';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter, RouteComponentProps, Redirect } from "react-router";

import './loginModal.css';
import { LOGIN_TOKEN } from '../../constants';

type IProps = RouteComponentProps & {
  getAuthToken: (code: string) => void;
}

class LoginModal extends Component<IProps> {
  constructor(props: IProps) {
    super(props);

  }
  onSuccess = (response: any) => {
    console.log("My Response Success", response.code);
    const { getAuthToken, history } = this.props
    getAuthToken(response.code)
    getGistUser();
    history.push("/notebooks")

  }
  onRequest = (response: any) => {
    console.log("MY REQUEST", response)
  }
  onFailure = (response: any) => {
    console.log("My Response Failure", response);
  }
  getAuth = () => {
    fetch(`http://localhost:3001/api/login/oauth/authorize`, {
      method: "GET",
      headers: {
        "Content-type": "application/json"
      }
    })
      // .then(response => response.json())
      .then(data => {
        // @ts-ignore
        console.log("MY DATA", data);
        /* dispatch(getSuccess(data)); */
      })
      .catch(err => {
        console.log("MY ERROR", err);
        /*  dispatch(getFailure(err)); */
      });
  }
  render() {
    return (
      <React.Fragment>
        {/* <Button onClick={this.getAuth}>GET AUTH TOKEN</Button> */}

        <GitHubLogin clientId="cecb634186711983aafd"
          className="gitLoginButton"
          onSuccess={this.onSuccess}
          onFailure={this.onFailure}
          onRequest={this.onRequest}
          redirectUri="http://localhost:3000/"
          scope="gist,user,repo"
        />
      </React.Fragment>

    );
  }
}
function mapStateToProps(state: any) {
  return {
    authToken: state.authToken
  }
}
function mapDispatchToProps(dispatch: any) {
  return bindActionCreators({
    getAuthToken: getAuthToken,
    getGistUser: getGistUser
  }, dispatch)
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginModal));