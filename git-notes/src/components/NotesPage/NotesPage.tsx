import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from "react-router";
import * as R from 'ramda';
import './NotePage.css';
import NotesPageInner from './NotesPageInner/NotesPageInner';
import AddNotesPageModal from './AddNotesPageModal/AddNotesPageModal';
import { bindActionCreators } from 'redux';
import { getGistById } from '../../actions';
import CircularProgress from '@material-ui/core/CircularProgress';
type IProps = RouteComponentProps & {
  gistList: any;
  isLoading: boolean
}
interface IState {
  addNotesModal: boolean;
  gistNoteByID: any;
}


class NotesPage extends React.Component<IProps, IState>{
  constructor(props: IProps) {
    super(props);
    this.state = {
      addNotesModal: false,
      gistNoteByID: []
    }
    this.addNotesModalClose = this.addNotesModalClose.bind(this);
    this.addNotesModalOpen = this.addNotesModalOpen.bind(this)
  }
  componentDidMount() {
    const { gistList, history } = this.props
    if (gistList.length !== 0) {
      this.gistDetailById()
    }
    else {
      history.push("/notebooks")
    }


  }
  gistDetailById() {
    const { gistList, match } = this.props;
    // @ts-ignore
    getGistById(match.params.gistId)
  }
  addNotesModalClose() {
    this.setState({
      addNotesModal: false
    })
  }
  addNotesModalOpen() {
    this.setState({
      addNotesModal: true
    })
  }
  render() {
    const { match, gistList, isLoading } = this.props
    // @ts-ignore
    /* console.log("FILTER", (R.find(R.propEq('id',match.params.gistId))(gistList))) */
    const { addNotesModal, gistNoteByID } = this.state
    return (
      <React.Fragment>
        {isLoading &&
          <CircularProgress className="progress" color="secondary" />
        }
        {gistList.length !== 0 && <NotesPageInner
          addNotesModalOpen={() => this.addNotesModalOpen()}
          // @ts-ignore
          gistNoteByID={R.find(R.propEq('id', match.params.gistId))(gistList)}
        />}
        <AddNotesPageModal
          addNotesModal={addNotesModal}
          addNotesModalClose={() => this.addNotesModalClose()}
          // @ts-ignore
          gistId={match.params.gistId}
        />
      </React.Fragment>

    );
  }
}
function mapStateToProps(state: any) {
  return {
    isLoading: state.gistList.isLoading,
    gistList: state.gistList.gistData
  }
}
function mapDispatchToProps(dispatch: any) {
  return bindActionCreators({
    getGistById: getGistById
  }, dispatch)
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NotesPage));