import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import NoteIcon from '@material-ui/icons/Notes'
import DeleteIcon from '@material-ui/icons/Delete'
import CreateIcon from '@material-ui/icons/Create'
//import Icon from '@material-ui/core/Icon';
import moment from 'moment';
import './NotesPageInner.css';
import Typography from '@material-ui/core/Typography';
interface IProps {
  addNotesModalOpen: () => void;
  gistNoteByID: any
}
function NotesPageInner(props: IProps) {
  const {
    addNotesModalOpen,
    gistNoteByID
  } = props;
  const {
    description,
    owner,
    files,
    created_at,
    updated_at,
  } = gistNoteByID

  console.log("LOCALSTORAGE", window.localStorage.getItem('gist-token'))
  let dataFiles = Object.values(files)
  return (
    <div>

      {(gistNoteByID.length !== 0) && <Grid container spacing={32}>

        <Grid item lg={10} className="notesPageGrid">
          <Paper className="notesPageDiv">
            <Grid container spacing={8} style={{ padding: "0 10px" }}>
              <Grid item lg={12} className="">
                <h2 className="headingNotes">Notebook Detail</h2>
              </Grid>
              <Grid item lg={12} className="notesItemDetail">
                <Grid container spacing={8}>
                  <Grid item lg={3}>
                    <p>Title</p>
                    <p>{description}</p>
                  </Grid>
                  <Grid item lg={3}>
                    <p>Owner</p>
                    <p>{owner.login}</p>
                  </Grid>
                  <Grid item lg={3}>
                    <p>Created At</p>
                    <p>{moment(created_at).format("MM/DD/YYYY")}</p>
                  </Grid>
                  <Grid item lg={3}>
                    <p>Updated At</p>
                    <p>{moment(updated_at).format("MM/DD/YYYY")}</p>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Grid item lg={10} className="notesPageGrid2">
          <Paper /* className="notesPaperDiv" */>
            <Grid container spacing={16}>
              <Grid item lg={12}>
                <h2 className="headingNotes">Notes</h2>
              </Grid>
              <Grid item lg={12}>
                {dataFiles.map((file: any, key:any) => (
                  <ExpansionPanel key={key} style={{ boxShadow: "none" }}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                      <NoteIcon />
                      <p style={{ marginLeft: "30px" }}>{file.filename}</p>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <Grid container>
                        <Grid item lg={12}>
                          <div className="expensionPanal">
                            <span><CreateIcon /></span>
                            <span><DeleteIcon /></span>
                          </div>
                        </Grid>
                        <Grid item lg={12}>
                          <div className="expensionPanalFile">
                            <Typography>
                              Documentation
                            </Typography>
                          </div>

                        </Grid>
                      </Grid>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                ))}

              </Grid>
              <Grid item lg={12} className="addButtonBottom">
                <Divider variant="middle" />
                <span onClick={() => addNotesModalOpen()}>
                  <Fab color="default" size="medium" aria-label="Add" className="addButtonFab">
                    <AddIcon />
                  </Fab>
                </span>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>}
    </div>
  );
}

export default NotesPageInner;