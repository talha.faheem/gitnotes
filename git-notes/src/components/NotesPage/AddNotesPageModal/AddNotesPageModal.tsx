import * as React from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import SaveImage from '@material-ui/icons/Save'
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { editGistFile } from '../../../actions';

interface IProps {
  addNotesModalClose: () => void;
  addNotesModal: boolean;
  editGistFile: (gistId: string, editObject: any) => void;
  gistId: string;
}
interface IState {
  title: string;
  content: string;
}
class AddNotesPageModal extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      title: "",
      content: ""
    }
  }
  titleChange = (e: any) => {
    this.setState({
      title: e.target.value
    })
  }
  contentChange = (e: any) => {
    this.setState({
      content: e.target.value
    })
  }
  editGistFile = ()=> {
    const { addNotesModalClose, gistId } = this.props
    
    let editObject = {
      title: this.state.title,
      content: this.state.content
    }
    console.log("EDIT ME", editObject);
    //this.props.editGistFile(gistId, editObject);
    addNotesModalClose()
  }
  render() {
    const {
      addNotesModalClose,
      addNotesModal
    } = this.props;
    return (
      <Dialog
        open={addNotesModal}
        onClose={() => addNotesModalClose()}
        aria-labelledby="responsive-dialog-title"
        className="dialogBox"
      >
        <DialogTitle id="customized-dialog-title">
          Create New NoteBook
          </DialogTitle>
        <DialogContent>
          <form>
            <div className="dialogFlex">
              <TextField
                id="fileName"
                label="File Name*"
                placeholder="File Name"
                margin="normal"
                variant="filled"
                onChange={this.titleChange}
              />

              <TextField
                id="content"
                label="Content"
                placeholder="Content"
                multiline
                rows="4"
                margin="normal"
                variant="filled"
                onChange={this.contentChange}
              />
            </div>
          </form>

        </DialogContent>
        <DialogActions>
          <Button
            className="newFileButton"
            variant="contained"
            color="secondary"
            onClick={() => this.editGistFile()}
            style={{ backgroundColor: "#c51162" }}>
            SAVE <SaveImage />
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
function mapDispatchToProps(dispatch: any) {
  return bindActionCreators({
    editGistFile: editGistFile
  }, dispatch)
}

export default connect(null, mapDispatchToProps)(AddNotesPageModal);