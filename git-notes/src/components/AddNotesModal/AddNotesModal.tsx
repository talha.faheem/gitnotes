import * as React from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import SaveImage from '@material-ui/icons/Save'
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import { addGist } from '../../actions'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

interface IProps {
  addModalClose: () => void;
  addModal: boolean;
  addGist: (createObject: any) => void;
}
interface IStates {
  title: string
}
class AddNotesModal extends React.Component<IProps, IStates> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      title: ""
    }
    this.titleChange = this.titleChange.bind(this);
    this.createGist = this.createGist.bind(this);
  }
  titleChange(e: any) {
    console.log("TITLE: - ",e.target.value)
    this.setState({
      title: e.target.value
    })
  }
  createGist() {
    const { addModalClose } = this.props
    console.log("CREATE ME");
    let createObject = {
      title: this.state.title
    }
    this.props.addGist(createObject);
    addModalClose()
  }
  render() {
    const {
      addModalClose,
      addModal
    } = this.props;
    return (
      <Dialog
        open={addModal}
        onClose={() => addModalClose()}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="customized-dialog-title">
          Create New NoteBook
          </DialogTitle>
        <DialogContent>
          <TextField
            id="filled-with-placeholder"
            label="Notebook Title*"
            placeholder="Notebook Title"
            margin="normal"
            variant="filled"
            onChange={this.titleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button
            className="newFileButton"
            variant="contained"
            color="secondary"
            onClick={() => this.createGist()}
            style={{ backgroundColor: "#c51162" }}>
            SAVE <SaveImage />
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

function mapDispatchToProps(dispatch: any) {
  return bindActionCreators({
    addGist: addGist
  }, dispatch)
}

export default connect(null, mapDispatchToProps)(AddNotesModal);