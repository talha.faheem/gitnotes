const express = require("express");
const app = express();
var cors = require("cors");
app.use(cors());
require("es6-promise").polyfill();
require("isomorphic-fetch");
app.use(express.json());
const GistClient = require("gist-client");
const gistClient = new GistClient();

app.post("/api/oauth/access-token", (req, res) => {
  console.log("HELLO");
  const code = req.body.code;
  // code in body
  console.log("here i am :", code);
  fetch(`https://github.com/login/oauth/access_token`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify({
      client_id: "cecb634186711983aafd",
      client_secret: "2b6daf5045376ce1a5875f90d7463a5da1de228e",
      code: code
    })
  })
    .then(response => response.text())
    .then(function(data) {
      console.log("MY DATA AUTH ", data);
      return res.json(data);
    })
    .catch(err => {
      console.log("ERROR ", err);
    });
});

app.post("/api/gist/user", (req, res) => {
  fetch("https://api.github.com/user", {
    method: "GET",
    headers: {
      Authorization: `token ${req.body.authToken}`,
      "Content-type": "application/json"
    }
  })
    .then(response => response.text())
    .then(function(data) {
      /* console.log("MY DATA AUTH ", data); */
      return res.json(data);
    })
    .catch(err => {
      console.log("ERROR ", err);
    });
});

app.get("/api/gists", (req, res) => {
  console.log("API POSTMAN ");
  //let authToken = req.body.authToken;
  console.log();
  res.json(gistClient.getAll());
});
app.get("/api/gists/user/:userName", (req, res) => {
  console.log(req.params.userName);

  fetch(`https://api.github.com/users/${req.params.userName}/gists`)
    .then(response => response.json())
    .then(data => {
      /* console.log(data); */
      return res.json(data);
    })
    .catch(err => {
      console.log(err);
    });
});

app.get("/api/gists/:gistId", (req, res) => {
  console.log("HELLO IBY ID");
  fetch(`https://api.github.com/gists/${req.params.gistId}`)
    .then(response => response.json())
    .then(data => {
      /* console.log(data); */
      return res.json(data);
    })
    .catch(err => {
      console.log(err);
    });
});
app.post("/api/gists", (req, res) => {
  console.log("REQUEST", req.body);
  fetch(`https://api.github.com/gists`, {
    method: "POST",
    headers: {
      Authorization: `token ${req.body.authToken}`,
      "Content-type": "application/json"
    },
    body: JSON.stringify({
      description: req.body.description,
      public: true,
      files: req.body.files
    })
  })
    .then(response => response.json())
    .then(data => {
      /* console.log(data) */
      return res.json(data);
    })
    .catch(err => {
      console.log(err);
    });
});

app.patch("/api/gists/:gistId", (req, res) => {
  fetch(`https://api.github.com/gists${req.param.gistId}`, {
    method: "PATCH",
    headers: {
      Authorization: `token ${req.body.authToken}`,
      "Content-type": "application/json"
    },
    body: JSON.stringify({
      description: req.body.description,
      public: true,
      files: req.body.files
    })
  })
    .then(response => response.json())
    .then(data => {
      return res.json(data);
    })
    .catch(err => {
      console.log(err);
    });
});

app.patch("/api/gist/file/:gistId", (req, res) => {
  fetch(`https://api.github.com/gists${req.param.gistId}`, {
    method: "PATCH",
    headers: {
      Authorization: `token ${req.body.authToken}`,
      "Content-type": "application/json"
    },
    body: JSON.stringify({
      files: req.body.files
    })
  })
    .then(response => response.json())
    .then(data => {
      return res.json(data);
    })
    .catch(err => {
      console.log(err);
    });
});

app.delete("/api/gists/:gistId", (req, res) => {
  console.log("DELETE API");
  fetch(`https://api.github.com/gists/${req.params.gistId}`, {
    method: "DELETE",
    headers: {
      Authorization: `token ${req.body.authToken}`,
      "Content-type": "application/json"
    }
  })
    .then(response => response.json())
    .then(data => {
      return res.json(data);
    })
    .catch(err => {
      console.log(err);
    });
});

const port = process.env.port || 3001;
app.listen(port, () => console.log(`Listening on port ${port}`));
